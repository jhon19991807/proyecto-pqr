import { Routes } from '@angular/router';
//Layouts
import {
  BlankComponent,
  ExecutiveLayout,
  DemoComponent
} from './@pages/layouts';

//Sample Pages
import { CondensedDashboardComponent } from './dashboard/condensed/dashboard.component';
import { SimplyWhiteDashboardComponent } from './dashboard/simplywhite/dashboard.component';
import { CasualDashboardComponent } from './dashboard/casual/dashboard.component';
import { CorporateDashboardComponent } from './dashboard/corporate/dashboard.component';
import { ExecutiveDashboardComponent } from './dashboard/executive/dashboard.component';
import { CardsComponentPage } from './cards/cards.component';
import { ViewsPageComponent } from './views/views.component';
import { ChartsComponent } from './charts/charts.component';
import { SocialComponent } from './social/social.component';

export const AppRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Home'
    },
    component: DemoComponent
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'menu_1',
        component: ExecutiveDashboardComponent,
        data: {
          title: 'dashboard'
        }
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'menu_2',
        component: SocialComponent,
        data: {
          title: 'social'
        }
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'menu_3',
        loadChildren: './extra/extra.module#ExtraModule'
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'layouts',
        loadChildren: './layouts/layouts.module#LayoutPageModule'
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'builder',
        loadChildren: './builder/builder.module#BuilderModule'
      }
    ]
  },
  {
    path: 'executive',
    component: BlankComponent,
    children: [
      {
        path: 'session',
        loadChildren: './session/session.module#SessionModule'
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'forms',
        loadChildren: './forms/forms.module#FormsPageModule'
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'ui',
        loadChildren: './ui/ui.module#UiModule'
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'email',
        loadChildren: './email/email.module#EmailModule'
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'cards',
        component: CardsComponentPage,
        data: {
          title: 'cards'
        }
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'views',
        component: ViewsPageComponent,
        data: {
          title: 'views'
        }
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'tables',
        loadChildren: './tables/tables.module#TablesModule'
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'maps',
        loadChildren: './maps/maps.module#MapsModule',
        data: {
          title: 'maps'
        }
      }
    ]
  },
  {
    path: 'executive',
    component: ExecutiveLayout,
    children: [
      {
        path: 'charts',
        component: ChartsComponent,
        data: {
          title: 'charts'
        }
      }
    ]
  }
];
