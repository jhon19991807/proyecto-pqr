import { Component, OnInit, Input } from '@angular/core';
import { SocialService } from './social.service';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss']
})
export class SocialComponent implements OnInit {

  constructor(private _service: SocialService) {}

  ngOnInit() {
    
  }
}
