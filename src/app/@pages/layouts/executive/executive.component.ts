import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RootLayout } from '../root/root.component';
declare var pg: any;
@Component({
  selector: 'executive-layout',
  templateUrl: './executive.component.html',
  styleUrls: ['./executive.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExecutiveLayout extends RootLayout implements OnInit {
  menuItems = [
    {
      label: 'Menu 1',
      details: '12 New Updates',
      routerLink: 'menu_1',
      iconType: 'pg',
      iconName: 'home'
    },
    {
      label: 'Menu 2',
      routerLink: 'menu_2',
      iconType: 'pg',
      iconName: 'social'
    },
    {
      label: 'Menu 3',
      routerLink: 'menu_3',
      iconType: 'pg',
      iconName: 'layouts'
    },
   
    {
      label: 'Menu 4',
      iconType: 'letter',
      iconName: 'Ui',
      toggle: 'close',
      mToggle: 'close',
      submenu: [
        {
          label: 'Item 1',
          routerLink: 'item_1',
          iconType: 'letter',
          iconName: 'c'
        },
        {
          label: 'Item 2',
          routerLink: 'item_2',
          iconType: 'letter',
          iconName: 't'
        },
        {
          label: 'Item 3',
          routerLink: 'item_3',
          iconType: 'letter',
          iconName: 'i'
        },
        {
          label: 'Item 4',
          routerLink: 'item_4',
          iconType: 'letter',
          iconName: 'b'
        },
        {
          label: 'Item 5',
          routerLink: 'item_5',
          iconType: 'letter',
          iconName: 'n'
        },
        {
          label: 'Item 6',
          routerLink: 'item_6',
          iconType: 'letter',
          iconName: 'm'
        }
      ]
    },
    {
      label: ''
    }
  ];
  ngOnInit() {
    pg.isHorizontalLayout = true;
    this.changeLayout('horizontal-menu');
    this.changeLayout('horizontal-app-menu');
  }
}
